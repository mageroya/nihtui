#ifndef NIHTUI_HPP
#define NIHTUI_HPP

#include <string>
#include <vector>
#include <csetjmp>

// Magic Numbers
#define COLUMN_WIDTH 4

namespace NIH {

	class Command
	{
		friend class Console;

		public:
			Command(const std::string& cmd, const std::string& help);
			virtual ~Command();
			bool operator==(const NIH::Command& other);
			bool operator!=(const NIH::Command& other);
			operator std::string ();

		private:
			std::string m_command;
			std::string m_help;
			//std::vector<std::string> m_options;
			bool async { false };
	};

	class Console
	{
		public:
			virtual ~Console();
			static Console* get_instance(); 
			void run_blocking();
			bool register_command(const NIH::Command& cmd);
		protected:
			Console();
		private:
			enum Color
			{
				NONE = 0, BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE
			};

			static sigjmp_buf ctrlc_buf;

			static char** command_completion(const char* stem_text, int start, int end);
			static char* command_name_generator(const char* stem_text, int state);

			static void siginthandler(int param);
			inline std::string setColor(Console::Color, Console::Color);
			size_t split(const std::string &input, std::string &st, std::vector<std::string> &strs);
			void trim(std::string& s);

			void help();

			static std::vector<NIH::Command> command_vec;

			static Console* m_instance;
			bool m_running { false };
			std::string PROMPT { ">>> " };
	};

}

#endif
