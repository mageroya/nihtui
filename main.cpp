#include <iostream>
#include <string>
#include "nihtui.hpp"

/*
using std::endl;
using std::cout;
using std::string;
*/

int main()
{
	auto c = NIH::Console::get_instance();
	NIH::Command ncom("testcommand", "some thing that the user will want to do");
	c->register_command(ncom);
	NIH::Command tcom("print", "prints some stuff to your screen");
	c->register_command(tcom);
	c->run_blocking();
	return 0;
}

