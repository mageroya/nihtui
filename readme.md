### what?!
* nih: not invented here [syndrom]  
* tui: text user interface  

### whatfor?
this is the user interface I wrote for my bachelors thesis,
enhanced further by integrating the readline library. 
work in progress. consider this GPL, I just don't want to
deal with license stuff right now 

### why?
because I often want a basic ui for my projects.  
because I like to write my own support libs.  
for practice.
