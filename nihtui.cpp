#include "nihtui.hpp"
#include <iostream>
#include <string>
#include <csignal>
#include <sstream>
#include <cstdlib>
#include <readline/readline.h>
#include <readline/history.h>

using std::endl;
using std::cout;
using std::string;

/* ===================================== CONSOLE CLASS IMPLEMENTATION ===================================== */

NIH::Console* NIH::Console::m_instance { nullptr };
std::vector<NIH::Command> NIH::Console::command_vec;
sigjmp_buf NIH::Console::ctrlc_buf;

inline string NIH::Console::setColor(Console::Color foreground = Console::Color::WHITE, Console::Color background = Console::Color::BLACK) {
	std::stringstream s;
	s << "\033[";
	if (!foreground && !background) { s << "0"; }// reset colors if no params
	if (foreground) { s << 29 + foreground; if (background) s << ";"; }
	if (background) { s << 39 + background; }
	s << "m";
	return s.str();
}

NIH::Console* NIH::Console::get_instance() {
	if (m_instance == nullptr)
		m_instance = new Console();
	return m_instance;
}

void NIH::Console::siginthandler(int param)
{
	if (param == SIGINT)
	{
		siglongjmp(NIH::Console::ctrlc_buf, 1); // DARK MAGIC
		cout << endl;
	}
}

NIH::Console::Console() {
	//rl_bind_key ('\t', rl_insert);
	//rl_bind_key('\t', rl_complete); // auto tabcomplete paths
	//rl_catch_signals = 1;
	//rl_set_signals();
	
	rl_attempted_completion_function = Console::command_completion;
	if (signal(SIGINT, NIH::Console::siginthandler) == SIG_ERR) {
		std::cerr << "failed to register interrupts\n";
		exit(1);
	}

	command_vec.push_back(NIH::Command("help", "display this command reference"));
	command_vec.push_back(NIH::Command("quit", "exit the application"));
	command_vec.push_back(NIH::Command("clear", "clear the terminal screen"));
}
NIH::Console::~Console()
{
	command_vec.clear();
}

bool NIH::Console::register_command(const NIH::Command& cmd)
{
	for (auto c : command_vec)
	{
		if (c == cmd)
			return false;
	}
	command_vec.push_back(cmd);
	return true;
}

size_t NIH::Console::split(const string &input, string &st, std::vector<string> &strs) {
	size_t pos = input.find(' ');
	size_t initialPos = 0;
	strs.clear();
	// first string is command
	if (pos != string::npos) {
		st = input.substr(initialPos, pos - initialPos);
		initialPos = pos + 1;
		pos = input.find(' ', initialPos);
		// Decompose arguments
		while (pos != string::npos) {
			strs.push_back(input.substr(initialPos, pos - initialPos));
			initialPos = pos + 1;
			pos = input.find(' ', initialPos);
		}
		// Add the last one
		strs.push_back(input.substr(initialPos, std::min(pos, input.size()) - initialPos));
	} else {
		st = input;
	}
	return strs.size();
}

void NIH::Console::trim(string& s) {
	size_t p = s.find_first_not_of(" \t");
	s.erase(0, p);

	p = s.find_last_not_of(" \t");
	if (string::npos != p) s.erase(p + 1);
}

char** NIH::Console::command_completion(const char* stem_text, int start, int end) {
	char** matches = nullptr;

	if (end){} // only that the unused warning goes away.

	if (start == 0) {
		matches = rl_completion_matches(stem_text, command_name_generator);
	}
	return matches;
}

char* NIH::Console::command_name_generator(const char* stem_text, int state) {
	static int count;
	if (state == 0) {
		count = -1;
	}

	while (count < (int) Console::command_vec.size()-1) {
		count++;

		if ( command_vec[(size_t)count].m_command.substr(0, string(stem_text).size()) == string(stem_text)) {
			return strdup(command_vec.at((size_t)count).m_command.c_str());
		}
	}
	return nullptr;
}

void NIH::Console::help() {
	cout << setColor(BLUE, BLACK) << ">>> help" << setColor(WHITE, BLACK) << endl;
	cout << setColor(GREEN, BLACK) << "USAGE: [command] + ENTER" << endl << "Commands are: \t" << setColor(WHITE, BLACK) << endl;
	
	long unsigned int maxCommandLength = 0;
	std::string space = " ";
	
	for (NIH::Command t : command_vec)		// determine longest command we have to display
	{
		if(t.m_command.size() > maxCommandLength)
		{
			maxCommandLength = t.m_command.size();
		}
	}
		
	for (NIH::Command t : command_vec)
	{
		std::string gap;
		// determine horizontal offset to align help texts
		long unsigned int offset = maxCommandLength - t.m_command.size() + COLUMN_WIDTH;
		if(offset <= COLUMN_WIDTH)
		{
			offset = COLUMN_WIDTH;
		}
		// concatenate necessary space strings to form the gap that aligns the help text
		for(long unsigned int idx = 0; idx < offset; idx++)
		{
			gap += space;
		}
		cout << setColor(YELLOW, BLACK) << t.m_command << gap << setColor(WHITE, BLACK) << t.m_help << endl;
		gap.clear();
	}
}


void NIH::Console::run_blocking() {

	char* i_char = nullptr;
	string inp;

	// the dark side of the force is strong...
	while (sigsetjmp(ctrlc_buf, 1) != 0);

	bool running = true;
	while (running) {
		do {
			i_char = readline(PROMPT.c_str());
			if (i_char && *i_char)
				add_history (i_char);	
			if (i_char) {
				inp.assign(i_char);
				free (i_char);
				i_char = nullptr;
			} else {
				inp.clear();
			}
		} while (inp.length() == 0);

		string st;
		std::vector<string> args;
		trim(inp);
		split(inp, st, args);

		if (st == "quit" || st == "q") {
			cout << setColor(BLUE, BLACK) << ">>> quit" << setColor(WHITE, BLACK) << endl;
			running = false;
			continue;
		}
		if (st == "c" || st == "clear") {
			cout << setColor(BLUE, BLACK) << ">>> clear" << setColor(WHITE, BLACK) << endl;
			cout << "\033[H\033[J";
			continue;
		}
		if (st == "help" || st == "h") {
			help();
			continue;
		}

		bool t_found = false;
		for (NIH::Command s : command_vec) {
			if (st == (string)s) {
				cout << setColor(BLUE, BLACK) << ">>> " << s.m_command << setColor(WHITE, BLACK) << endl;
				t_found = true;
				break;
			}
		}
		if (t_found) { continue; }

		cout << setColor(RED, BLACK) << ">>> Command '" << st << "' not recognized." << endl;
		cout << ">>> Arguments were ";
		if (args.empty()) cout << "empty.";
		else {
			for (string s : args) {
				cout << "'" << s << "' ";
			}
		}
		cout << endl << ">>> Type 'help' to display valid commands." << setColor(WHITE, BLACK) << endl;

	} // end while

	cout << "\r                                                        \r";

	delete m_instance;
}

/* ===================================== COMMAND CLASS IMPLEMENTATION ===================================== */

bool NIH::Command::operator==(const NIH::Command& other) {
	return this->m_command == other.m_command;
}

bool NIH::Command::operator!=(const NIH::Command& other) {
	return this->m_command == other.m_command;
}

NIH::Command::Command(const std::string& cmd, const std::string& help) : m_command(cmd), m_help(help)
{ }

NIH::Command::~Command()
{ }

NIH::Command::operator std::string () {
	return m_command;
}
