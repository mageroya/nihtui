# this makefile should only be run with G++ versions 4.8.3 or above.

OBJS = main.o nihtui.o
#LIBOBJS = lib/*.o
LIBOBJS =
HEADS = nihtui.hpp

EXECUTABLE = nihtui.x
PACKFILE = nihtui_src
SIGNING_IDENTITY = bendt@hm.edu

CC = g++ -std=c++11 -m64
DEBUG = -g3 -O0
AFLAGS = -Wall -Wextra -Wconversion -Wsign-compare -Wshadow -Wzero-as-null-pointer-constant -pedantic $(DEBUG)
CFLAGS = -c -I../autonacl/include
LDFLAGS = -lreadline #-pthread #-L../autonacl/lib -lnacl 

#all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $(AFLAGS) -o $@ $^ $(LIBOBJS) $(LDFLAGS) 

%.o: %.cpp $(HEADS)
	$(CC) $(AFLAGS) $(CFLAGS) $<

.PHONY: tidy clean re pack verify

re:
	$(MAKE) clean
	$(MAKE) $(EXECUTABLE)
	$(MAKE) pack
	$(MAKE) verify

tidy: 
	rm -f *.o core

clean: tidy
	rm -f *.x *.gz *.sig

pack:
	tar cvf - *.cpp *.hpp makefile | pigz -p4 -9 > $(PACKFILE).tar.gz
	gpg -u $(SIGNING_IDENTITY) --output $(PACKFILE).tar.gz.sig --detach-sig $(PACKFILE).tar.gz
	tar cvf - *.tar.gz* | pigz -p4 -9 > $(PACKFILE).signed.tar.gz

verify:
	gpg --verify $(PACKFILE).tar.gz.sig $(PACKFILE).tar.gz

